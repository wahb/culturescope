response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.menu = [
(T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
(T('Settings'),URL('default','settings')==URL(),URL('default','settings'),[]),
(T('Online Nodes'),URL('default','nodes')==URL(),URL('nodes','index'),[]),
(T('Live Plot'),URL('default','sensors')==URL(),URL('live','index'),[]),
(T('Reports'),URL('default','reports')==URL(),URL('reports','index'),[]),
(T('Device info'),URL('default','reports')==URL(),URL('default','device'),[]),
]