import requests

__auth__ = ('wahbishak', '123321')
__host__ = 'http://127.0.0.1'

def post(p, auth):
    '''
    required parameters :
        @mac            : node MAC Address 
        @label          : sensor label (Accel, Temp, Light ... etc)
        @s_value        : sensor transmitted value
    optional parameters (any) :
        @name           : node label
        @description    : node description
        @n_type         : node type/model e.g. TI__CC2650STK-Wireless_MCU
        @update_rate    : in [ms]
        @is_online      : defined automatically by the system and should not defined by the user
        @s_time         : default value request.now (web2py) but can be defined by the user
        @s_description  : sensor description
    format :
        {'parameter', value}
    example:
        minimal -> {
            'mac':'00:0a:95:9d:68:16',
            's_value':30,
            'label':'Accel'
        }
        complete -> {
            'name':'My experiment',
            'description':'perfect measurement for my experiment',
            'n_type':'TI__CC2650STK-Wireless_MCU',
            'mac':'00:0a:95:9d:68:16',
            'update_rate':30,
            'label':'Accel',
            's_description':'detect sample movement',
            's_value':30
        }
    '''
    r = requests.post('%s/culturescope/system/api/pi_nodes.json' % __host__ , data=p, auth=auth)
    if r.status_code == requests.codes.ok:
        return r.content
    return r.status_code

def get(p, auth):
    '''
    parameters :
        @table    : pi_nodes or pi_data
        @id       : [id] or last to show last inserted record
        @field    : field name of pi_nodes or pi_data, check models/db_wizard.py
    example :
        p -> {
            'table':'pi_nodes',
            'id':16,
            'field':'mac'
        }
    '''
    r = requests.get('%s/culturescope/system/get.json' % __host__, params=p, auth=auth)
    if r.status_code == requests.codes.ok:
        return r.content
    return r.status_code

def get_as_rest(url, auth):
    '''
    You can use RESTful patterns to gather data
    format: 
        URL -> http://127.0.0.1/culturescope/system/api/nodes
        URL -> http://127.0.0.1/culturescope/system/api/data
        URL -> http://127.0.0.1/culturescope/system/api/node/[id]
        URL -> http://127.0.0.1/culturescope/system/api/node/[id]/data
        URL -> http://127.0.0.1/culturescope/system/api/node/[id]/data/[field]
        URL -> http://127.0.0.1/culturescope/system/api/node/[id]/data/[label]
        URL -> http://127.0.0.1/culturescope/system/api/node/[id]/data/[label]/[field]
    you can use the pattern 'last' at the end of the URL to get the last record :
        e.g. URL -> http://127.0.0.1/culturescope/system/api/nodes/last

    example :
        get @s_value of the last @sensor Accel inserted by @node 16:
            URL -> http://127.0.0.1/culturescope/system/api/node/16/data/Accel/s_value/last
        get all nodes:
            URL -> http://127.0.0.1/culturescope/system/api/nodes
        get all data of @node 16:
            URL -> http://127.0.0.1/culturescope/system/api/node/16/data
    '''
    r = requests.get(url, auth=auth)
    if r.status_code == requests.codes.ok:
        return r.content
    return r.status_code

if __name__ == "__main__":
    auth = __auth__
    pp = {
            'mac':'00:0a:95:9d:68:16',
            's_value':30,
            'label':'Accel'
        }
    pg = {
            'table':'pi_nodes',
            'id':16,
            'field':'mac'
        }
    print "POST", post(pp, auth)
    print "GET", get(pg, auth)
    print "RESTful GET", get_as_rest('http://127.0.0.1/culturescope/system/api/node/16/data', auth)