from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'culture.scope'
settings.subtitle = 'powered by web2py'
settings.author = 'Wahb Ben Ishak'
settings.author_email = 'wahbishak@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '062b7692-9b28-4774-b354-0dc908c7b45d'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
