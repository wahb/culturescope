# -*- coding: utf-8 -*-
# perform json requests
### required - do no delete
response.generic_patterns = ['*']
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@request.restful()
def api():
    response.view = 'generic.json'
    def GET(*args,**vars):
        return dict()
    def POST(*args,**vars):
        return dict()
    def PUT(*args,**vars):
        return dict()
    def DELETE(*args,**vars):
        return dict()
    return locals()

@auth.requires_login()
def nodes():
    mac = None
    node_id = -1
    node = {}
    data = {}
    if request.vars.mac:
        mac = db.pi_nodes(mac=request.vars.mac)
        node_id = mac.id if mac is not None else node_id
        if mac is None:
            for var in request.vars:
                if var in db.pi_nodes.fields:
                    node[var] = request.vars[var]
                if var in db.pi_data.fields:
                    data[var] = request.vars[var]
            node_id = db.pi_nodes.bulk_insert([node])[0]
            data_id = db.pi_data.bulk_insert([node])[0]
    return dict(status=response.status, node_id=node_id)

def data():
    return dict()

