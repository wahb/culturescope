

var $hc = function (sensor, json, urt, live) {
    console.log(json)
    $(document).ready(function () {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        $.getJSON(json, function(){}).done(function(data){
        
        $('#container').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function() {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        if (live == true) {
                            setInterval(function () {
                                var x = (new Date()).getTime(), // current time
                                y = data['content']["s_value"];
                                series.addPoint([x, y], true, true);
                            }, urt);
                        } else {
                            for (var index in data) {
                                var x = new Date(data['s_time']),
                                y = data["s_value"];
                                series.addPoint([x, y], true, true);
                            }
                        }
                    }
                }
            },
            title: {
                text: sensor
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: 'data',
                data: (function () {
                    var output = []
                    if (live == true){
                        // generate an array of random data
                            var time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            output.push({
                                x: time + i * urt,
                                y: data['content']["s_value"]
                            });
                        }
                    } else {
                        for (var index in data) {
                            output.push({
                                x: new Date(data[index]['s_time']),
                                y: data[index]["s_value"]
                            });
                        }
                    }
                        return output;
                    }())
            }]
        });
        })
        .fail(function(value){
        console.log('fail')
        $('#container').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                }
            },
            title: {
                text: sensor
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'data',
                data: []
            }]
        });
    });
    });
}

var $onLive = function(){
        var node = $('#no_table_node').val()
        var sensor = $('#no_table_sensor').val()
        var json = '/culturescope/system/api/node/'+node+'/data/'+sensor+'/s_value/last.json'
        $hc(sensor, json, 500, true)
}

var $onReport = function(){
        var node = $('#node').val()
        var sensor = $('#sensor').val()
        var start = $('#start').val()
        var stop = $('#stop').val()
        var json = '/culturescope/system/getdata.json?start='+start+'&stop='+stop+'&node='+node+'&label='+sensor
        $hc(sensor, json, 0, false)
}

$(function () {
    $('#no_table_node').change($onLive);
    $('#no_table_sensor').change($onLive);
});

$(function () {
    //$('#node').change($onReport);
    //$('#sensor').change($onReport);
    $('#submit_report').click($onReport);
});