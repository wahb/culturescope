# -*- coding: utf-8 -*-
# perform json requests
### required - do no delete
import re
response.generic_patterns = ['*']
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@request.restful()
@auth.requires_login()
def api():
    response.view = 'generic.json'
    def GET(*args,**vars):
        patterns = [
            "/nodes[pi_nodes]",
            "/nodes[pi_nodes]/last",
            "/nodes[pi_nodes]/:field",
            "/nodes[pi_nodes]/:field/last",
            "/node[pi_nodes]/{pi_nodes.id}",
            "/node[pi_nodes]/{pi_nodes.id}/:field",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/last",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/:field",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/:field/last",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/{pi_data.label}",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/{pi_data.label}/last",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/{pi_data.label}/:field",
            "/node[pi_nodes]/{pi_nodes.id}/data[pi_data.node]/{pi_data.label}/:field/last",
            "/data[pi_data]",
            "/data[pi_data]/last",
            "/data/{pi_data.label.startswith}",
            "/data/{pi_data.label}/:field",
            "/data/{pi_data.label}/node[pi_nodes]",
            "/data/{pi_data.label}/node[pi_nodes]/{pi_nodes.mac}",
            "/data/{pi_data.label}/node[pi_nodes]/{pi_nodes.mac}/:field"
            ]
        
        parser = db.parse_as_rest(patterns,args,vars)
        if parser.status == 200:
            if 'last' in args:
                return dict(content=parser.response[-1])
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status,parser.error)
    def POST(table,**fields):
        if table not in db: raise HTTP(400)
        if table == 'pi_nodes':
            insert = postnodes(fields)
            return dict(node_id=insert[0],data_id=insert[1])
        if table == 'pi_data':
            insert = postdata(fields)
            return dict(data_id=insert)
        return dict()
    return locals()

def get():
    id = 0
    table = request.vars.table
    if request.vars.id: id = request.vars.id
    field = request.vars.field
    if table not in db:
        raise HTTP(400)
    if id == 'last':
        if field:
            if field not in db[table].fields: 
                raise HTTP(400)
            else:
                return {'%s' % field:db(db[table].id > 0).select(db[table][field]).last()[field]}
        return db(db[table].id > 0).select().last().as_dict()
    elif id == 0:
        if field:
            if field not in db[table].fields: 
                raise HTTP(400)
            else:
                return db(db[table].id > 0).select(db[table].id, db[table][field]).as_dict()
        return db(db[table].id > 0).select().as_dict()
    elif re.match('[0-9]', str(id)):
        if field:
            if field not in db[table].fields: 
                raise HTTP(400)
            else:
                return db(db[table].id == id).select(db[table].id, db[table][field]).as_dict()
        return db(db[table].id == id).select().as_dict()
    else:
        raise HTTP(400)

def postnodes(fields):
    dict = {}
    node_id = -1
    data_id = -1

    try:
        node = db.pi_nodes(mac=fields['mac'])

        if node is None:
            for var in db.pi_nodes.fields:
                if var in fields.keys():
                    dict[var] = fields[var]
            node_id = db.pi_nodes.bulk_insert([dict])[0]
        else:
            node_id = node.id

        data_id = postdata(fields)

        return [node_id, data_id]
    except:
        raise HTTP(400)

def postdata(fields):
    dict = {}
    data_id = -1

    try:
        node = db.pi_nodes(mac=fields['mac'])

        if node:
            print node.id
            dict['node'] = node.id
            for var in db.pi_data.fields:
                if var in fields.keys():
                    dict[var] = fields[var]
            data_id = db.pi_data.bulk_insert([dict])

        return data_id
    except:
        raise(400)

def getdata():
    start = 0
    stop = request.now
    node = True
    label = True
    if request.vars.node: node = db.pi_data.node == request.vars.node
    if request.vars.label: label = db.pi_data.label == request.vars.label
    if request.vars.start: start = request.vars.start
    if request.vars.stop: stop = request.vars.stop
    field = request.vars.field
    if field in db.pi_data.fields:
        return db((node)
                 &(label)
                 &(db.pi_data.s_time >= start)
                 &(db.pi_data.s_time <= stop)).select(db.pi_data.id, db.pi_data[field]).as_dict()
    else:
        return db((node)
                 &(label)
                 &(db.pi_data.s_time >= start)
                 &(db.pi_data.s_time <= stop)).select().as_dict()