# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    form = SQLFORM.factory(
            Field('node', 'reference pi_nodes', requires=IS_IN_DB(db,db.pi_nodes.id,'%(mac)s'),
                label=T('Node')),
            Field('sensor', 'reference pi_data', requires=IS_IN_SET(['Accel','Temp','Humidity','Light']),
                label=T('Sensor'))
            )
    return dict(form=form)

def error():
    return dict()

