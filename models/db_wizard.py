### we prepend  to tablenames and  to fieldnames for disambiguity

########################################

db.define_table('pi_licence',
    Field('licence_key', type='upload', required=True,
          label=T('Licence Key')),
    Field('assinged_to', type='string', writable=False,
          label=T('Assigned to')),
    Field('mac', type='string', writable=False,
          label=T('MAC')),
    Field('is_active', type='boolean', writable=False,
          label=T('Is active ?')),
    Field('expiring_on', type='datetime', writable=False,
          label=T('Expiring on')),
    auth.signature,
    format='%(licence_key)s',
    migrate=settings.migrate)

########################################

db.define_table('pi_device',
    Field('name', type='upload', writable=False,
          label=T('name')),
    Field('os', type='string', writable=False,
          label=T('OS')),
    Field('firmware', type='string', writable=False,
          label=T('firmware')),
    Field('ip', type='string', writable=False,
          label=T('IP')),
    Field('mac', type='string', writable=False,
          label=T('MAC')),
    auth.signature,
    format='%(name)s',
    migrate=settings.migrate)

########################################

db.define_table('pi_settings',
    Field('ip', type='string', required=True,
          label=T('IP')),
    Field('port', type='integer', required=True,
          label=T('Port')),
    Field('mac', type='string', required=True,
          label=T('MAC')),
    Field('password', type='password',
          label=T('password')),
    Field('licence_key', 'reference pi_licence',
          label=T('Licence Key')),
    auth.signature,
    format='%(ip)s',
    migrate=settings.migrate)

########################################
db.define_table('pi_nodes',
    Field('name', type='string', default=None,
          label=T('Name')),
    Field('description', type='text', default=None,
          label=T('Description')),
    Field('n_type', type='string', writable=False, default=None,
          label=T('Type')),
    Field('mac', type='string', writable=False, required=True,
          label=T('MAC')),
    Field('update_rate', type='double', writable=False, default=30,
          label=T('Update rate')),
    Field('is_online', type='boolean', default=True, writable=False, readable=False,
          label=T('Is online ?')),
    auth.signature,
    format='%(name)s',
    migrate=settings.migrate)

########################################
db.define_table('pi_data',
    Field('node', 'reference pi_nodes',
          label=T('Node')),
    Field('label', type='string', required=True,
          label=T('Label')),
    Field('s_description', type='string',
          label=T('Description')),
    Field('s_value', type='double', required=True,
          label=T('Value')),
    Field('s_time', type='datetime', default=request.now,
          label=T('Time')),
    auth.signature,
    format='%(label)s',
    migrate=settings.migrate)

#db.define_table('pi_sensors_archive',db.pi_sensors,Field('currenrecord','reference pi_sensors',readable=False,writable=False))
