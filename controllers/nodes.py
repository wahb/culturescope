# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    record = SQLFORM.grid(db.pi_nodes.is_online==True, 
    deletable=True,
    editable=True,
    details=False,
    create=False)
    return {'record':record}

def error():
    return dict()

