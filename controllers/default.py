# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    return dict()

def error():
    return dict()

@auth.requires_login()
def settings():
    form=SQLFORM(db.pi_settings)
    return dict(form=form)

@auth.requires_login()
def device():
    form=SQLFORM(db.pi_device)
    return dict(form=form)

